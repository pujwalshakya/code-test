<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
});
Route::get('car','carController@index');
Route::post('car/save','carController@saveData');
Route::post('car/update','carController@updateData');
Route::get('car/delete/{id}','carController@deleteData');

Route::get('parts','partsController@index');
Route::post('parts/save','partsController@saveData');
Route::post('parts/update','partsController@updateData');
Route::get('parts/delete/{id}','partsController@deleteData');

Route::get('factory','factoryController@index');
Route::get('factory/part_detail/{id}','factoryController@partsDetail');
Route::post('factory/parts/submit','factoryController@updatePartsDetail');
