<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\partsModel;

class factoryController extends Controller
{
    public function index(){
    	$parts=partsModel::all();
    	return view('factory.factory',compact('parts'));
    }
    public function partsDetail($id){
    	$parts=partsModel::find($id);
    	return view('factory.partsDetail',compact('parts'));
    }
    public function updatePartsDetail(Request $request){
    	$result=partsModel::find($request->id)->update(['quantity'=> $request->rem_quantity]);
    	return redirect()->back();
    }
}
