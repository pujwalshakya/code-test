<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\partsModel;

class partsController extends Controller
{
    public function index(){
    	$parts=partsModel::all();
    	return view('parts.parts_list',compact('parts'));
    }
    public function saveData(Request $request){
    	$result=partsModel::create([
    		'factory_id'=>1,
    		'name'=>$request->name,
    		'rate'=>$request->rate,
    		'quantity'=>$request->quantity,
    		'price'=>$request->price,
    		'nepali_date'=>$request->date
    	]);
    	return redirect()->back();

    }
    public function updateData(Request $request){
    	$result=partsModel::find($request->id)->update([
    		'factory_id'=>1,
    		'name'=>$request->name,
    		'rate'=>$request->rate,
    		'quantity'=>$request->quantity,
    		'price'=>$request->price,
    		'nepali_date'=>$request->date
    	]);
    	return redirect()->back();

    }
    public function deleteData($id){
    	$result=partsModel::find($id)->delete();
    }
    


}
