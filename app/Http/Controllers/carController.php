<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\carModel;

class carController extends Controller
{
    public function index(){
        $cars=carModel::all();
    	return view('car.car_list',compact('cars'));
    }
    public function saveData(Request $request){
    	$result=carModel::create([
    		'name'=>$request->car_name,
    		'model_no'=>$request->model_no,
    		'cost'=>$request->cost,
    		'date'=>$request->date
    	]);
    	return redirect()->back();
    }
    public function updateData(Request $request){ 
        $result=carModel::find($request->car_id)->update([
            'name'=>$request->car_name,
            'model_no'=>$request->model_no,
            'cost'=>$request->cost,
            'date'=>$request->date        
        ]);
        return redirect()->back();
    }
    public function deleteData($id){
        $result=carModel::find($id)->delete();
    }
}
