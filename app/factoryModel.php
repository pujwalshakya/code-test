<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class factoryModel extends Model
{
    protected $table= 'factory';
    protected $fillable=['parts_name','rate','available_quantity','required_quantity','required_quantity'];

    public function parts(){
    	return $this->hasMany(partsModel::class,'factory_id');
    }
}
