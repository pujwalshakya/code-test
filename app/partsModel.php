<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class partsModel extends Model
{
    protected $table= 'parts';
    protected $fillable= ['factory_id','name','rate','quantity','price','nepali_date'];

    public function factory(){
    	return $this->belongsTo(factoryModel::class,'factory_id');
    }
}

