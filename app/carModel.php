<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class carModel extends Model
{
   protected $table= 'cars';
   protected $fillable= ['name','model_no','cost','date'];
}
