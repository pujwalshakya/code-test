<div id="sidebar" class="sidebar responsive ace-save-state">
	<script type="text/javascript">
		try{ace.settings.loadState('sidebar')}catch(e){}
	</script>

	<ul class="nav nav-list">
		<li >
			<a href="{{asset('car')}}">
				<i class="fa fa-hand-o-right"></i>
				<span class="menu-text"> Car </span>
			</a>

			<b class="arrow"></b>
		</li>

		

		<li class="">
			<a href="{{asset('parts')}}">
				<i class="fa fa-hand-o-right"></i>
				<span class="menu-text"> Parts</span>
			</a>

			<b class="arrow"></b>
		</li>

		<li class="">
			<a href="{{asset('factory')}}">
				<i class="fa fa-hand-o-right"></i>
				<span class="menu-text"> Factory </span>
			</a>

			<b class="arrow"></b>
		</li>

	</ul><!-- /.nav-list -->

	<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
		<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
	</div>
</div>