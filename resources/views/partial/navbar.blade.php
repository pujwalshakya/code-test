<div id="navbar" class="navbar navbar-default ace-save-state">
	<div class="navbar-container ace-save-state" id="navbar-container">
		<div class="navbar-header pull-left">
			<a href="index.html" class="navbar-brand">
				<small>
					<i class="fa fa-leaf"></i>
					Just A Try
				</small>
			</a>
		</div>

		<div class="navbar-buttons navbar-header pull-right" role="navigation">
			<ul class="nav ace-nav">

				<li class="light-blue dropdown-modal">
					<a data-toggle="dropdown" href="#" class="dropdown-toggle">
						
						<span class="fa fa-user">
							Settings
						</span>

						<i class="ace-icon fa fa-caret-down"></i>
					</a>

					<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
						<!-- <li>
							<a href="#">
								<i class="ace-icon fa fa-user"></i>
								Change Password
							</a>
						</li> -->
						<!-- <li class="divider"></li>
						<li>
							<a href="profile.html">
								<i class="ace-icon fa fa-user"></i>
								User Manual
							</a>
						</li>
						<li class="divider"></li>
						<li>
							<a href="profile.html">
								<i class="ace-icon fa fa-user"></i>
								Developer Detail
							</a>
						</li> -->

						<!-- <li class="divider"></li> -->

						
				</ul>
			</li>
		</ul>
	</div>
</div><!-- /.navbar-container -->
</div>