{{csrf_field()}}
<div id="list_search_result" class="col-md-12">
		<legend class="center">PARTS DETAILS</legend>
	<div class="col-xs-5">
		<div class="table-responsive">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td>Parts Name</td>
						<td>{{$parts->name}}</td>
						<input type="hidden" name="name" value="{{$parts->name}}">
						<input type="hidden" name="id" value="{{$parts->id}}">
					</tr>
					<tr>
						<td>Rate</td>
						<td>{{$parts->rate}}</td>
						<input type="hidden" name="rate" value="{{$parts->rate}}">
					</tr>
					<tr>
						<td>Available Quantity</td>
						<td>{{$parts->quantity}}</td>
						<input type="hidden" name="quantity" id="available" value="{{$parts->quantity}}">
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-xs-7">
		<div class="table-responsive">
			<fieldset>
				<div class="form-group">
					<label class="control-label col-md-4">Required Quantity</label>
					<div class="col-md-6">
						<input type="text" name="req_quantity" class="form-control" id="req_quantity" onkeyup="calculation()" required/>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-4">Remaining Quantity</label>
					<div class="col-md-6">
						<input type="text" name="rem_quantity" class="form-control" id="rem_quantity" readonly="">
					</div>
				</div>
				<div class="hr hr-18 dotted hr-double"></div>
				<div class="col-md-8 col-md-offset-4">
					<div class="col-md-4">
						<input type="submit" name="save_detail" value="Submit" class="btn btn-success col-md-12" >
					</div>
					<div class="col-md-4">
						<a href="{{asset('factory')}}" class="btn btn-danger col-md-12">Cancel</a>
					</div>
				</div>
			</fieldset>
		</div>
	</div>
</div>
