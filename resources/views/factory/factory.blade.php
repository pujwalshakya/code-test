@extends('layout.app')
@push('stylesheet')
<link rel="stylesheet" href="{{asset('css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" href="{{asset('css/chosen.min.css')}}" />
<!-- <link rel="stylesheet" type="text/css" href="nepali.datepicker.css"> -->

@endpush
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="{{asset('dashboard')}}">Factory</a>
        </li>

        <li>
            <a href="{{asset('marga')}}">Parts List</a>
        </li>
        
    </ul><!-- /.breadcrumb -->
</div>
<div class="page-content">
	<div class="row">
		<div class="col-xs-12 well">
			<div class="col-md-6">
				<legend> Select Part Below for detail</legend>
				<select class="chosen-select form-control" id="parts" name="parts" 
				onChange="myfunction($(this).val())">
					<option value="" disabled="disable" selected="selected">Please Select Part</option>
					@foreach($parts as $part)
						<option value="{{$part->id}}">{{$part->name}}</option>
					@endforeach
				</select>
				<div class="hr hr-double hr-dotted hr18"></div>
			</div>
			<form role="form" class="form-horizontal" action="{{asset('factory/parts/submit')}}" 
			method="post"  id="parts_detail">
			
			<div id="list_search_result" class="col-md-12">
				<div class="col-xs-5">
					<div class="table-responsive">
						<table class="table table-bordered table-striped" >
							
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('script')
<script src="{{asset('js/chosen.jquery.min.js')}}"></script>
<script>
	if(!ace.vars['touch']) {
		$('.chosen-select').chosen({allow_single_deselect:true});
	}

	function myfunction(id){
		$.ajax({
			url:"{{asset('factory/part_detail')}}/"+id,
			type:'get',
			success:function(response){
				$('#parts_detail').html(response);
			}
		})
	}
	function calculation(){
		var available= $('#available').val();
		var req= $('#req_quantity').val();
		if(req.length==0){
			req=0;
		}
		var rem=available-req;
		$('#rem_quantity').val(rem);
	}
</script>
@endpush