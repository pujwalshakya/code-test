@extends('layout.app')
@push('stylesheet')
<link rel="stylesheet" href="{{asset('css/bootstrap-datepicker3.min.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('css/nepali.datepicker.v2.2.min.css')}}">

@endpush
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="{{asset('dashboard')}}">Car</a>
        </li>

        <li>
            <a href="{{asset('marga')}}">Car List</a>
        </li>
        
    </ul><!-- /.breadcrumb -->
</div>
<div class="page-content">
    <div class="row">
    	<div class="well">
    		<button role="button" class="btn btn-success" data-toggle="modal" data-target="#add_car-modal">
                  <i class="fa fa-plus"></i>
                  Add Car
              </button>
               <div class="hr hr-double hr-dotted hr18"></div>
            <div class="table-responsive" id="list-staff">
                <table class="table table-striped table-bordered table-hover table-resposnive" id="marga">
                    <thead>
                        <th>S.No</th>
                        <th>Cars Name</th>
                        <th>Model No.</th>
                        <th>Price</th>
                        <th>Delivery Date</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                        @foreach($cars as $key=>$car)
                            <tr id='{{$car->id}}'>
                                <td>{{$key+1}}</td>
                                <td>{{$car->name}}</td>
                                <td>{{$car->model_no}}</td>
                                <td>{{$car->cost}}</td>
                                <td>{{$car->date}}</td>
                                <td>
                                    <div class="btn-group">
                                        <a class="edit-faculty btn btn-xs btn-warning" 
                                    onclick="UpdateCar({{$car}})">
                                        <span class="ace-icon fa fa-edit bigger-120"></span>
                                        Update Detail
                                    </a>
                                    <a role="button" class="btn btn-xs btn-danger remove_marga" onclick="Delete({{$car->id}})" id="{{$car->id}}">
                                        <span class="ace-icon fa fa-trash-o bigger-120"></span>
                                        Delete
                                    </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
    	</div>
    </div>
</div>

<!-- car model starts -->

<div id="add_car-modal" class="modal fade"  tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{asset('car/save')}}" id="modalform" method="post" class="form-horizontal clearfix" role="form" >
                {{csrf_field()}}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="blue bigger">Please fill the following form fields</h4>
                </div>
				<div class="modal-body">
					<div class="row">
							<div class="form-group col-sm-12">
								<label box for="form-field-username" class="col-sm-4 col-form-label">Car Name</label>
                                <div class="col-sm-8">
								    <input type="text" id="form-field-username" name="car_name" required="" />
                                </div>
							</div>
							<div class="form-group col-sm-12">
								<label for="form-field-username" class="col-sm-4 col-form-label">Model Number</label>
                                <div class="col-sm-8">
								    <input type="text" id="form-field-username" name="model_no" required="" />
                                </div>
							</div>
							<div class="form-group col-sm-12">
								<label for="form-field-username" class="col-sm-4 col-form-label">Cost</label>
                                <div class="col-sm-8">
								    <input type="number" id="form-field-username" name="cost" required="" />
                                </div>
							</div>
							<div class="form-group col-sm-12">
								<label class="col-sm-4 col-form-label">Delivery Date<span style="color: red;">*</span></label>	
                                <div class="col-sm-8">						
								    <input  type="text" class="date-picker" id="date" name="date" placeholder="yyyy-mm-dd" required />
                                </div>
							</div>
					</div>
				</div>
                <div class="modal-footer">
                    <button class="btn btn-sm" data-dismiss="modal">
                        <i class="ace-icon fa fa-times"></i>
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-sm btn-primary" id="car_submit">
                        <i class="ace-icon fa fa-check" ></i>
                        Save
                    </button>
                </div>
        	</form>
        </div>
    </div>
</div>

    <!-- end car modal -->

<!-- update modal starts -->
<div id="update_car-modal" class="modal fade"  tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{asset('car/update')}}" id="modalform" method="post" class="form-horizontal clearfix" role="form" >
                {{csrf_field()}}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="blue bigger">Update the following form fields</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                            <div class="form-group col-sm-12">
                                <input type="hidden" id="car_id" name="car_id">
                                <label box for="form-field-username" class="col-sm-4 col-form-label">Car Name</label>
                                <div class="col-sm-8">
                                    <input type="text" id="car_name" name="car_name" required="" />
                                </div>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="form-field-username" class="col-sm-4 col-form-label">Model Number</label>
                                <div class="col-sm-8">
                                    <input type="text" id="car_model" name="model_no" required="" />
                                </div>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="form-field-username" class="col-sm-4 col-form-label">Cost</label>
                                <div class="col-sm-8">
                                    <input type="number" id="car_price" name="cost" required="" />
                                </div>
                            </div>
                            <div class="form-group col-sm-12">
                                <label class="col-sm-4 col-form-label">Delivery Date<span style="color: red;">*</span></label>  
                                <div class="col-sm-8">                      
                                    <input  type="text" class="date-picker" id="d_date" name="date" readonly="" />
                                </div>
                            </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm" data-dismiss="modal">
                        <i class="ace-icon fa fa-times"></i>
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-sm btn-primary" id="car_submit">
                        <i class="ace-icon fa fa-check" ></i>
                        Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- update modal ends -->
@endsection
@push('script')
<!-- nepali datepicker -->
<script src="{{asset('js/nepali.datepicker.v2.min.js')}}"></script>
<script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('js/jquery.validate.min.js')}}"></script>
<!-- <script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="nepali.datepicker.js"></script> -->
<script>

// $('#date').datepicker({
// 		format:'yyyy-mm-dd',
// 		autoclose: true,
// 		todayHighlight:true
// 	});
$('#date').nepaliDatePicker({
	onFocus: false,
	ndpTriggerButton: true,
	ndpTriggerButtonText: 'Date',
	ndpTriggerButtonClass: 'btn btn-primary btn-sm'
});
function UpdateCar(response){
    $('#car_name').val(response.name);
    $('#car_model').val(response.model_no);
    $('#car_price').val(response.cost);
    $('#d_date').val(response.date);
    $('#car_id').val(response.id);
    $('#update_car-modal').modal();

}
function Delete(id){
    if(confirm('Are you sure want to delete?')==false){
        return false;
    }else{
        $.ajax({
            url:"{{asset('car/delete')}}/"+id,
            type:'get',
            success:function(response){
                $('#'+id).remove();
            }
        })
    }
}
$('#modalform').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },
    });
</script>
@endpush