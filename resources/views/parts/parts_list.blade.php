@extends('layout.app')
@push('stylesheet')
<link rel="stylesheet" href="{{asset('css/bootstrap-datepicker3.min.css')}}" />
<!-- <link rel="stylesheet" type="text/css" href="nepali.datepicker.css"> -->

@endpush
@push('stylesheet')
<link rel="stylesheet" href="{{asset('css/bootstrap-datepicker3.min.css')}}" />
<!-- <link rel="stylesheet" type="text/css" href="nepali.datepicker.css"> -->

@endpush
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="{{asset('dashboard')}}">Car</a>
        </li>

        <li>
            <a href="{{asset('marga')}}">Car List</a>
        </li>
        
    </ul><!-- /.breadcrumb -->
</div>
<div class="page-content">
    <div class="row">
    	<div class="well">
    		<button role="button" class="btn btn-success" data-toggle="modal" data-target="#add_parts-modal">
                <i class="fa fa-plus"></i>
                  Add Parts
            </button>
            <div class="hr hr-double hr-dotted hr18"></div>
            <div class="table-responsive" id="list-staff">
				<table class="table table-striped table-bordered table-hover table-resposnive" id="marga">
					<thead>
						<th>S.No</th>
						<th>Name</th>
						<th>Rate</th>
						<th>Quantity</th>
						<th>Total Price</th>
						<th>Purchase Date</th>
						<th>Action</th>
					</thead>
					<tbody>
						@foreach($parts as $key=>$part)
							<tr id='{{$part->id}}'>
								<td>{{$key+1}}</td>
								<td>{{$part->name}}</td>
								<td>{{$part->rate}}</td>
								<td>{{$part->quantity}}</td>
								<td>{{$part->price}}</td>
								<td>{{$part->nepali_date}}</td>
								<td>
									<div class="btn-group">
										<a class="edit-faculty btn btn-xs btn-warning" 
                                    onclick="UpdatePart({{$part}})">
                                        <span class="ace-icon fa fa-edit bigger-120"></span>
                                        Update Detail
                                    </a>
                                    <a role="button" class="btn btn-xs btn-danger remove_marga" onclick="Delete({{$part->id}})" id="{{$part->id}}">
                                        <span class="ace-icon fa fa-trash-o bigger-120"></span>
                                        Delete
                                    </a>
									</div>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>	
            </div>
    	</div>
    </div>
</div>


<!-- parts add modal -->
<div id="add_parts-modal" class="modal fade"  tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{asset('parts/save')}}" id="modalform" method="post" class="form-horizontal clearfix" role="form" >
                {{csrf_field()}}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="blue bigger">Please fill the following form fields</h4>
                </div>
				<div class="modal-body">
					<div class="row">
						<div class="center">
							<div class="form-group">
								<label for="form-field-username">Parts Name</label>
								<div>
									<input type="text" id="form-field-username" name="name" required="" />
								</div>
							</div>
							<div class="form-group">
								<label for="form-field-username">Rate</label>
								<div>
									<input type="number" id="rate" name="rate" onkeyup="calculation()" required=""  />
								</div>
							</div>
							<div class="form-group">
								<label for="form-field-username">Quantity</label>
								<div>
									<input type="number" id="quantity" name="quantity" onkeyup="calculation()" required=""  />
								</div>
							</div>
							<div class="form-group">
								<label for="form-field-username">Price</label>
								<div>
									<input type="number" id="price" name="price" readonly=""  />
								</div>
							</div>
							<div class="form-group">
								<label>Purchase Date<span style="color: red;">*</span></label>								
							<div>
								<input  type="text" class="date-picker" id="date" name="date" placeholder="yyyy-mm-dd" required />
							</div>
							</div>
						</div>
					</div>
				</div>
                <div class="modal-footer">
                    <button class="btn btn-sm" data-dismiss="modal">
                        <i class="ace-icon fa fa-times"></i>
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-sm btn-primary" id="car_submit">
                        <i class="ace-icon fa fa-check" ></i>
                        Save
                    </button>
                </div>
        	</form>
        </div>
    </div>
</div>
<!-- modal end -->

<!-- update modal starts -->
<div id="update-modal" class="modal fade"  tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{asset('parts/update')}}" id="modalform" method="post" class="form-horizontal clearfix" role="form" >
                {{csrf_field()}}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="blue bigger">Please fill the following form fields</h4>
                </div>
				<div class="modal-body">
					<div class="row">
						<div class="center">
							<div class="form-group">
								<input type="hidden" name="id" id="parts_id">
								<label for="form-field-username">Parts Name</label>
								<div>
									<input type="text" id="parts_name" name="name" required="" />
								</div>
							</div>
							<div class="form-group">
								<label for="form-field-username">Rate</label>
								<div>
									<input type="number" id="parts_rate" name="rate" onkeyup="calculation()" required=""  />
								</div>
							</div>
							<div class="form-group">
								<label for="form-field-username">Quantity</label>
								<div>
									<input type="number" id="parts_quantity" name="quantity" onkeyup="calculation()" required=""  />
								</div>
							</div>
							<div class="form-group">
								<label for="form-field-username">Price</label>
								<div>
									<input type="number" id="parts_price" name="price" readonly=""  />
								</div>
							</div>
							<div class="form-group">
								<label>Purchase Date<span style="color: red;">*</span></label>								
							<div>
								<input  type="text" class="date-picker" id="n_date" name="date" readonly="" placeholder="yyyy-mm-dd" required />
							</div>
							</div>
						</div>
					</div>
				</div>
                <div class="modal-footer">
                    <button class="btn btn-sm" data-dismiss="modal">
                        <i class="ace-icon fa fa-times"></i>
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-sm btn-primary" id="car_submit">
                        <i class="ace-icon fa fa-check" ></i>
                        Save
                    </button>
                </div>
        	</form>
        </div>
    </div>
</div>

<!-- update modal ends -->
@endsection

@push('script')
<!-- nepali datepicker -->
<script src="{{asset('js/nepali.datepicker.v2.min.js')}}"></script>
<script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
<!-- <script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="nepali.datepicker.js"></script> -->
<script>

$('#date').datepicker({
		format:'yyyy-mm-dd',
		autoclose: true,
		todayHighlight:true
	});
// $('#d_o_j').nepaliDatePicker({
// 	onFocus: false,
// 	ndpTriggerButton: true,
// 	ndpTriggerButtonText: 'Date',
// 	ndpTriggerButtonClass: 'btn btn-primary btn-sm'
// });

function calculation(){
	var rate=$('#rate').val();
	if(rate.length==0){
		rate=0;
	}
	var quantity=$('#quantity').val();
	if(quantity.length==0){
		quantity=0;
	}
	var total=rate*quantity;
	$('#price').val(total);
	var u_rate=$('#parts_rate').val();
	if(u_rate.length==0){
		u_rate=0;
	}
	var u_quantity=$('#parts_quantity').val();
	if(u_quantity.length==0){
		u_quantity=0;
	}
	var u_total=u_rate*u_quantity;
	$('#parts_price').val(u_total);
}

function UpdatePart(response){
	$('#parts_name').val(response.name);
	$('#parts_rate').val(response.rate);
	$('#parts_quantity').val(response.quantity);
	$('#parts_price').val(response.price);
	$('#n_date').val(response.nepali_date);
	$('#parts_id').val(response.id);
	$('#update-modal').modal();

}
function Delete(id){
	if(confirm('Are you sure want to delete?')==false){
		return false;
	}else{
		$.ajax({
			url:"{{asset('parts/delete')}}/"+id,
			type:'get',
			success:function(response){
				$('#'+id).remove();
			}
		})
	}
}

</script>
@endpush